#include "summoning.hpp"

#include <components/debug/debuglog.hpp>

#include "../mwbase/environment.hpp"
#include "../mwbase/world.hpp"
#include "../mwbase/mechanicsmanager.hpp"

#include "../mwworld/esmstore.hpp"
#include "../mwworld/class.hpp"
#include "../mwworld/manualref.hpp"
#include "../mwworld/inventorystore.hpp"

#include "../mwrender/animation.hpp"

#include "creaturestats.hpp"
#include "aifollow.hpp"

namespace MWMechanics
{
    bool isSummoningEffect(const int effectId)
    {
        return ((ESM::MagicEffect::SummonScamp <= effectId && effectId <= ESM::MagicEffect::SummonStormAtronach)
            || (effectId == ESM::MagicEffect::SummonCenturionSphere)
            || (ESM::MagicEffect::SummonFabricant <= effectId && effectId <= ESM::MagicEffect::SummonCreature05));
    }

    std::string getSummonedCreature(const int effectId)
    {
        static const std::map<int, std::string> summonMap
        {
            {ESM::MagicEffect::SummonAncestralGhost, "sMagicAncestralGhostID"},
            {ESM::MagicEffect::SummonBonelord, "sMagicBonelordID"},
            {ESM::MagicEffect::SummonBonewalker, "sMagicLeastBonewalkerID"},
            {ESM::MagicEffect::SummonCenturionSphere, "sMagicCenturionSphereID"},
            {ESM::MagicEffect::SummonClannfear, "sMagicClannfearID"},
            {ESM::MagicEffect::SummonDaedroth, "sMagicDaedrothID"},
            {ESM::MagicEffect::SummonDremora, "sMagicDremoraID"},
            {ESM::MagicEffect::SummonFabricant, "sMagicFabricantID"},
            {ESM::MagicEffect::SummonFlameAtronach, "sMagicFlameAtronachID"},
            {ESM::MagicEffect::SummonFrostAtronach, "sMagicFrostAtronachID"},
            {ESM::MagicEffect::SummonGoldenSaint, "sMagicGoldenSaintID"},
            {ESM::MagicEffect::SummonGreaterBonewalker, "sMagicGreaterBonewalkerID"},
            {ESM::MagicEffect::SummonHunger, "sMagicHungerID"},
            {ESM::MagicEffect::SummonScamp, "sMagicScampID"},
            {ESM::MagicEffect::SummonSkeletalMinion, "sMagicSkeletalMinionID"},
            {ESM::MagicEffect::SummonStormAtronach, "sMagicStormAtronachID"},
            {ESM::MagicEffect::SummonWingedTwilight, "sMagicWingedTwilightID"},
            {ESM::MagicEffect::SummonWolf, "sMagicCreature01ID"},
            {ESM::MagicEffect::SummonBear, "sMagicCreature02ID"},
            {ESM::MagicEffect::SummonBonewolf, "sMagicCreature03ID"},
            {ESM::MagicEffect::SummonCreature04, "sMagicCreature04ID"},
            {ESM::MagicEffect::SummonCreature05, "sMagicCreature05ID"}
        };

        const auto it = summonMap.find(effectId);
        if (it != summonMap.end())
            return MWBase::Environment::get().getWorld()->getStore().get<ESM::GameSetting>().find(it->second)->mValue.getString();
        return std::string();
    }

    UpdateSummonedCreatures::UpdateSummonedCreatures(const MWWorld::Ptr& actor)
        : mActor(actor)
    {
    }

    void UpdateSummonedCreatures::visit(const EffectKey key, const int effectIndex, const std::string& sourceName, const std::string& sourceId, int casterActorId, const float magnitude, float remainingTime, float totalTime)
    {
        if (isSummoningEffect(key.mId) && magnitude > 0)
        {
            mActiveEffects.insert(ESM::SummonKey(key.mId, sourceId, effectIndex));
        }
    }

    void UpdateSummonedCreatures::process(const bool cleanup)
    {
        auto& creatureStats = mActor.getClass().getCreatureStats(mActor);
        auto& creatureMap = creatureStats.getSummonedCreatureMap();

        for (const auto& mActiveEffect : mActiveEffects)
        {
            {
                auto found = creatureMap.find(mActiveEffect) != creatureMap.end();
                if (found)
                    continue;
            }
            auto creatureID = getSummonedCreature(mActiveEffect.mEffectId);
            if (creatureID.empty())
                continue;
            auto creatureActorId = -1;
            try
            {
                MWWorld::ManualRef ref(MWBase::Environment::get().getWorld()->getStore(), creatureID, 1);

                auto& summonedCreatureStats = ref.getPtr().getClass().getCreatureStats(ref.getPtr());

                // Make the summoned creature follow its master and help in fights
                AiFollow package(mActor);
                summonedCreatureStats.getAiSequence().stack(package, ref.getPtr());
                creatureActorId = summonedCreatureStats.getActorId();

                auto placed = MWBase::Environment::get().getWorld()->safePlaceObject(
                    ref.getPtr(), mActor, mActor.getCell(), 0, 120.f);

                auto* anim = MWBase::Environment::get().getWorld()->getAnimation(placed);
                if (!anim)
                    continue;

                const auto* fx = MWBase::Environment::get().getWorld()->getStore().get<ESM::Static>()
                                                           .search("VFX_Summon_Start");
                if (fx)
                    anim->addEffect("meshes\\" + fx->mModel, -1, false);
            }
            catch (std::exception& e)
            {
                Log(Debug::Error) << "Failed to spawn summoned creature: " << e.what();
                // still insert into creatureMap so we don't try to spawn again every frame, that would spam the warning log
            }

            creatureMap.emplace(mActiveEffect, creatureActorId);
        }

        // Update summon effects
        for (auto it = creatureMap.begin(); it != creatureMap.end();)
        {
            auto found = mActiveEffects.find(it->first) != mActiveEffects.end();
            if (!found)
            {
                // Effect has ended
                MWBase::Environment::get().getMechanicsManager()->cleanupSummonedCreature(mActor, it->second);
                creatureMap.erase(it++);
            }
            else
                ++it;
        }

        auto graveyard = creatureStats.getSummonedCreatureGraveyard();
        creatureStats.getSummonedCreatureGraveyard().clear();

        for (const auto creature : graveyard)
            MWBase::Environment::get().getMechanicsManager()->cleanupSummonedCreature(mActor, creature);

        if (!cleanup)
            return;

        for (auto it = creatureMap.begin(); it != creatureMap.end();)
        {
            auto ptr = MWBase::Environment::get().getWorld()->searchPtrViaActorId(it->second);
            if (ptr.isEmpty() || (ptr.getClass().getCreatureStats(ptr).isDead() && ptr.getClass().getCreatureStats(ptr).
                isDeathAnimationFinished()))
            {
                // Purge the magic effect so a new creature can be summoned if desired
                const auto& key = it->first;
                creatureStats.getActiveSpells().purgeEffect(key.mEffectId, key.mSourceId, key.mEffectIndex);
                creatureStats.getSpells().purgeEffect(key.mEffectId, key.mSourceId);
                if (mActor.getClass().hasInventoryStore(mActor))
                    mActor.getClass().getInventoryStore(mActor).purgeEffect(
                        key.mEffectId, key.mSourceId, false, key.mEffectIndex);

                MWBase::Environment::get().getMechanicsManager()->cleanupSummonedCreature(mActor, it->second);
                creatureMap.erase(it++);
            }
            else
                ++it;
        }
    }
}
